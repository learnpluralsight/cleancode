package m7classes;

import java.util.ArrayList;
import java.util.List;

public class CouplingExample1 {

    ArrayList<String> list = new ArrayList<>();

    void doSomething(ArrayList<String> list){
        String firstElement = list.get(0);

        //do something first element
    }

    void doSomethingElse(ArrayList<String> list){
        String lastElement = list.get(list.size() - 1);

        //do something first element
    }

    // Programming to an Interface
    List<String> list2 = new ArrayList<>();


    void doSomething2(List<String> list){
        String firstElem = list.get(0);

        // do something with firstElem
    }

    void doSomethingElse2(List<String> list){
        String lastElem = list.get(list.size() - 1);

        // do something with lastElem
    }
}
