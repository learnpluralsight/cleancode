package m7classes;

import java.io.File;
import java.io.InputStream;

public class CouplingExample2 {

    public int calculateSomething(File source){
        //open InputStream from file
        return 0;
    }

    public int calculateSomething2(InputStream inputStream){
        //read inputStream
        return 0;
    }
}
