package m3naming;

import java.time.Instant;
import java.time.ZoneId;

public class DateUtils {
    public static void printNowNewYorkTime(){
        String now = Instant.now().atZone(ZoneId.of("America/New_York")).toString();
        System.out.println(now);
    }
}
